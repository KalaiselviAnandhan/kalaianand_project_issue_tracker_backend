const express = require("express");
const app = express();
const cors = require("cors");
const projects = require("./Routes/projects");
const table = require("./Connection/tableCreation");
const PORT = process.env.PORT || 3000;
const checkvalid = require("./Routes/checkvalid");
const logger = require('./logger')

async function tablesCreation(){
    try{
        await table.projectTableCreation();
        await table.issueTableCreation();
        await table.commentTableCreation();
        logger.info('Tables are Created...')
    }
    catch(error){
        console.error(error);
        logger.error('Error occurred while creating the tables...')
    }
}
tablesCreation()

app.use(cors())
app.use(express.json())

app.use('/projects',projects)
app.use('/checkvalid',checkvalid)

app.listen(PORT);