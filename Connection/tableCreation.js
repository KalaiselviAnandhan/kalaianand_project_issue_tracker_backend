const pool = require("./connection")
const logger = require('../logger')

const projects = `CREATE TABLE IF NOT EXISTS projects(project_id INT NOT NULL PRIMARY KEY,project_name TEXT,project_description TEXT, project_owner TEXT);`

const issues = `CREATE TABLE IF NOT EXISTS issues(id INT NOT NULL PRIMARY KEY,issue_id INT NOT NULL, issue_title TEXT,username TEXT,issue_description TEXT,issue_status TEXT,issue_severity TEXT, issue_classification TEXT, issue_project_id INT NOT NULL,FOREIGN KEY(issue_project_id) REFERENCES projects(project_id) ON UPDATE CASCADE ON DELETE CASCADE,UNIQUE(issue_id, issue_project_id));`

const comments = `CREATE TABLE IF NOT EXISTS comments(id INT NOT NULL PRIMARY KEY,comment_id INT NOT NULL,user_name TEXT,comment TEXT,comment_issue_id INT NOT NULL,comment_issue_project_id INT NOT NULL,FOREIGN KEY(comment_issue_id,comment_issue_project_id) REFERENCES issues(issue_id, issue_project_id) ON UPDATE CASCADE ON DELETE CASCADE, UNIQUE(comment_id,comment_issue_id,comment_issue_project_id));`

const projectValues = `INSERT INTO projects VALUES(1, 'Counter App For Shopping Cart','This tiny application is like adding,removing,deleting, resetting products which reflects in the total number of products into our shopping cart.','Karthick'),(2, 'Movie Rentals','This application displays the details about the movies and ratings','Anand');`

const issueValues = `INSERT INTO issues VALUES( 1, 1,'Issue with enabling of reload button','Palani','Reload button not working when user click the button','Closed','Minor','UI/Usability',1),(2,2,'Live URL not working','Gowtham','The LIVE URL for the demo is not working anymore','Closed','Minor','UI/Usability',1),(3,1,'URL not working','Sethu','The LIVE URL for the demo is not working anymore','Closed','Minor','UI/Usability',2);`

const commentValues = `INSERT INTO comments VALUES(1,1,'Karthick','I think its working. just check once again.',1,2),(2,1,'Karthick','I think its working. just check once again.',1,1);`


function projectTableCreation()
{
    return new Promise((resolve,reject)=>{
        pool.query(projects, (error)=>{
            if(error){
                logger.error(error)
                reject(error)
            }
            pool.query(`SELECT COUNT(*) AS COUNTER FROM projects`,(error,result)=>{
                if(error){ 
                    logger.error(error)
                    reject(error)
                }
                if(result[0]['COUNTER']=== 0){
                    pool.query(projectValues, (error)=>{
                        if(error){
                            logger.error(error)
                            reject(error)
                        }
                        logger.info('Project Table Created..')
                        resolve()
                    })
                }
            })
        })
    })
}

function issueTableCreation()
{
    return new Promise((resolve,reject)=>{
        pool.query(issues,(error)=>{
            if(error){
                logger.error(error)
                reject(error)
            }
            pool.query(`SELECT COUNT(*) AS COUNTER FROM issues`,(error,result)=>{
                if(error){
                    logger.error(error)
                    reject(error)
                }
                if(result[0]['COUNTER']=== 0){
                    pool.query(issueValues,(error)=>{
                        if(error){
                            logger.error(error)
                            reject(error)
                        }
                        logger.info('Issue Table Created..')
                        resolve()
                    })
                }
            })
        })
    })
}

function commentTableCreation()
{
    return new Promise((resolve,reject)=>{
        pool.query(comments,(error)=>{
            if(error){
                logger.error(error)
                reject(error)
            }
            pool.query(`SELECT COUNT(*) AS COUNTER FROM comments`,(error,result)=>{
                if(error) {
                    logger.error(error)
                    reject(error)
                }
                if(result[0]['COUNTER']=== 0){
                    pool.query(commentValues,(error)=>{
                        if(error){
                            logger.error(error)
                            reject(error)
                        }
                        logger.info('Comment Table Created..')
                        resolve()
                    })
                }
            })
        })
    })
}

module.exports = {
    projectTableCreation,
    issueTableCreation,
    commentTableCreation
}
