const {Router} = require("express");
const pool = require("../Connection/connection");
const check = new Router();
const logger = require('../logger')

check.get('/project/:id', (req,res)=>{
    const matchId = `SELECT project_name FROM projects WHERE project_id = ${req.params.id};`
    pool.query(matchId,(error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        else{
            logger.info('Validate the Project Id')
            (!result.length) ? res.send("not exist") : res.status(422).send("exist")
        }                            
    })
})

check.get('/issue/:projectid/:issueid', (req,res)=>{
    const matchId = `SELECT issue_title FROM issues WHERE issue_project_id = ${req.params.projectid} AND issue_id = ${req.params.issueid};`
    pool.query(matchId,(error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        else{
            logger.info('Validate the Project Id and Issue Id')
            (!result.length) ? res.send("not exist") : res.status(422).send("exist")
        }                            
    })
})

check.get('/comment/:projectid/:issueid/:commentid', (req,res)=>{
    const matchId = `SELECT comment FROM comments WHERE comment_issue_project_id = ${req.params.projectid} AND comment_issue_id = ${req.params.issueid} AND comment_id = ${req.params.commentid};`
    pool.query(matchId,(error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        else{
            logger.info('Validate the Project Id, Issue Id and Comment Id')
            (!result.length) ? res.send("not exist") : res.status(422).send("exist")
        }                            
    })
})

module.exports = check