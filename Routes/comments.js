const pool = require("../Connection/connection")
const comment = require("express").Router()
const { check, validationResult } = require("express-validator")
const checkEmpty = (value) => check(value).isLength({min:1}).withMessage(`${value} Required`)
const logger = require('../logger')


comment.get('/',(req,res)=>{
    const query = `SELECT * FROM comments WHERE comment_issue_id = ${req.issueId} AND comment_issue_project_id = ${req.projectId}`
    pool.query(query, (error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        logger.info('Fetched all the Comments')
        res.send(result)
    })
})

comment.post('/',[
    checkEmpty('comment_id'),
    checkEmpty('user_name'),
    checkEmpty('comment')
],(req,res)=>{
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        logger.error(error)
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        let lastIdQuery= `SELECT id FROM comments ORDER BY id DESC LIMIT 1`
        const insert="INSERT INTO comments (id,comment_id,user_name,comment,comment_issue_id,comment_issue_project_id) VALUES (?,?,?,?,?,?)"
        const matchId = `SELECT user_name FROM comments WHERE comment_issue_project_id = ${req.projectId} AND comment_issue_id = ${req.issueId} AND comment_id = ${req.body.comment_id}`
        let lastId=1;

        pool.query(matchId,(error,checkId)=>{
            if(error){
                logger.error(error)
                res.status(500).send("Internal Server Error")
            }
            if(!checkId.length){
                pool.query(lastIdQuery,(error,result)=>{
                    if(error){
                        logger.error(error)
                        res.status(500).send("Internal Server Error")
                    }
                    else{
                        lastId += Number(result[0].id)
                        pool.query(insert,[lastId,req.body.comment_id,req.body.user_name,req.body.comment,req.issueId,req.projectId],(error,finalResult)=>{
                            if(error){
                                logger.error(error)
                                res.status(500).send("Internal Server Error")
                            }
                            logger.info("Created a new Comment")
                            res.send(finalResult)
                        })
                    }
                })
            }
            else{
                logger.error('Comment id already exist..')
                res.status(422).send("Unprocessable Entity")
            }
        })
    }
})

comment.put('/:commentId',[
    checkEmpty('user_name'),
    checkEmpty('comment')
],(req,res)=>{
    const errors = validationResult(req)
    if (!errors.isEmpty()) {  
        logger.error(error)
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const query = `UPDATE comments SET user_name = ?, comment = ? WHERE comment_issue_id = ? AND comment_issue_project_id = ?`
        pool.query(query,[req.body.user_name,req.body.comment,req.issueId,req.projectId],(error,result)=>{
            if(error){
                logger.error(error)
                res.status(500).send("Internal Server Error")
            }
            logger.info('Updated a Comment')
            res.send(result)
        })
    }
})

comment.delete('/:commentId',(req,res)=>{
    const query = `DELETE FROM comments WHERE comment_issue_project_id = ${req.projectId} AND comment_issue_id = ${req.issueId} AND comment_id = ${req.params.commentId}`
    pool.query(query,(error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        logger.info('Deleted a Comment')
        res.send(result)
    })
})

module.exports = comment