const issue = require("express").Router()
const comment =require("./comments")
const pool = require("../Connection/connection")
const { check, validationResult } = require("express-validator")
const logger = require('../logger')

const checkEmpty = (value) => check(value).isLength({min:1}).withMessage(`${value} Required`)

issue.get('/', (req,res)=>{
    const query = `SELECT * FROM issues WHERE issue_project_id = ${req.projectId}`
    pool.query(query, (error,result)=>{
        if(error) {
            logger.error(error)
            console.log(error)
            res.status(500).send("Internal Server Error")
        }
        logger.info('Fetched all the Issues')
        res.send(result)
    })
})

issue.post('/', [
    checkEmpty('issue_id'),
    checkEmpty('issue_title'),
    checkEmpty('username'),
    checkEmpty('username'),
    checkEmpty('issue_description'),
    checkEmpty('issue_status'),
    checkEmpty('issue_severity'),
    checkEmpty('issue_classification')
],(req,res)=>{
    const errors = validationResult(req)
    if (!errors.isEmpty()) {    
        logger.error(error)
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const matchId = `SELECT issue_title FROM issues WHERE issue_id = ${req.body.issue_id} AND issue_project_id = ${req.projectId}`
        const maxIdQuery = `SELECT id FROM issues ORDER BY id DESC LIMIT 1`
        const query = `INSERT INTO issues(id,issue_id,issue_title,username,issue_description,issue_status,issue_severity,issue_classification,issue_project_id) VALUES (?,?,?,?,?,?,?,?,?)`
        let maxId = 1;
        pool.query(matchId,(error,checkId)=>{
            if(error){
                logger.error(error)
                res.status(500).send("Internal Server Error")
            }
            if(!checkId.length){
                pool.query(maxIdQuery,(error,result)=>{
                    if(error){ 
                        logger.error(error)
                        res.status(500).send("Internal Server Error")
                    }
                    else{
                        maxId+=Number(result[0].id)
                        pool.query(query,[maxId,req.body.issue_id,req.body.issue_title,req.body.username,req.body.issue_description,req.body.issue_status,req.body.issue_severity,req.body.issue_classification,req.projectId],(error,finalResult)=>{
                            if(error){
                                logger.error(error)
                                res.status(500).send("Internal server Error")
                            }
                            logger.info('Created a new Issue')
                            res.send(finalResult)
                        })
                    }
                })
            }
            else{
                logger.error('Issue id already Exist..')
                res.status(422).send("Unprocessable Entity")
            }
        })
    }
})

issue.put('/:issueId',[
    checkEmpty('issue_title'),
    checkEmpty('username'),
    checkEmpty('issue_description'),
    checkEmpty('issue_status')
],(req,res)=>{
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        logger.error(error)
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const query = `UPDATE issues SET issue_title = ?, username = ?, issue_description = ?, issue_status = ? WHERE issue_project_id = ? AND issue_id = ?`

        pool.query(query,[req.body.issue_title,req.body.username,req.body.issue_description,req.body.issue_status, req.projectId, req.params.issueId], (error,result)=>{
            if(error){
                logger.error(error)
                res.status(500).send("Internal Server Error")
            }
            logger.info('Updated a Issue')
            res.send(result)
        })
    }
})

issue.delete('/:issueId',(req,res)=>{
    const query = `DELETE FROM issues WHERE issue_project_id = ${req.projectId} AND issue_id = ${req.params.issueId}`

    pool.query(query, (error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        logger.info('Deleted a Issue')
        res.send(result)
    })
})

issue.use('/:issueId/comments',(req,res,next)=>{
    req.issueId = req.params.issueId
    next()
},comment)
module.exports = issue