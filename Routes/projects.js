const project = require("express").Router()
const issue = require("./issues")
const pool = require('../Connection/connection')
const { check, validationResult } = require("express-validator")
const logger = require('../logger')

const checkEmpty = (value) => check(value).isLength({min:1}).withMessage(`${value} Required`)

project.get("/",(req,res)=>{
    const query = `SELECT * FROM projects`
    pool.query(query, (error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        logger.info('Fetched all the Projects')
        res.send(result)
    })
})


project.post("/", [
    checkEmpty('project_id'),
    checkEmpty('project_name'),
    checkEmpty('project_description'),
    checkEmpty('project_owner')
], (req,res) =>{
    const errors = validationResult(req)
    if (!errors.isEmpty()) { 
        logger.error(error)
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const query = "INSERT INTO projects(project_id,project_name,project_description,project_owner)VALUES (?,?,?,?)"
        const matchId = `SELECT project_name FROM projects WHERE project_id = ${req.body.project_id}`
        pool.query(matchId, (err,checkId)=> {
            
            if(err){
                logger.error(error)
                res.status(500).send("Internal Server Error")
            }
            if(!checkId.length){
                pool.query(query, [req.body.project_id,req.body.project_name,req.body.project_description,req.body.project_owner], (error,result)=>{
                    if(error){
                        logger.error(error)
                        res.status(500).send("Internal Server Error")
                    }
                    logger.info('Created a new Project')
                    res.send(result)
                })
            }
            else{
                logger.error('Project id already exist..')
                res.status(422).send("Unprocessable Entity")
            }
        })
    }
})

project.put("/:projectId",[
    checkEmpty('project_name'),
    checkEmpty('project_owner')
],(req,res)=>{
    const errors = validationResult(req)
    if (!errors.isEmpty()) {    
        logger.error(error)
        return res.status(422).json({ errors: errors.array() })   
    }
    else{
        const query = `UPDATE projects SET project_name = ?, project_description = ?, project_owner = ? WHERE project_id = ?`
        pool.query(query,[req.body.project_name, req.body.project_description,req.body.project_owner, req.params.projectId], (error,result)=>{
            if(error){
                logger.error(error)
                res.status(500).send("Internal Server Error")
            }
            logger.info('Updated a Project')
            res.send(result)
        })
    }
})

project.delete("/:projectId",(req,res)=>{
    const query = `DELETE FROM projects WHERE project_id = ${req.params.projectId}`
    pool.query(query, (error,result)=>{
        if(error){
            logger.error(error)
            res.status(500).send("Internal Server Error")
        }
        logger.info('Deleted a Project')
        res.send(result)
    })
})

project.use("/:projectId/issues",(req,res,next)=>{
    req.projectId = req.params.projectId
    next()
},issue)

module.exports = project