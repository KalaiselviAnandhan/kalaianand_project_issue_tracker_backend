const { createLogger, transports, format } = require('winston')

const customFormat = format.combine(
    format.timestamp(),
    format.simple()
)
const info = new transports.File({
    filename:'logger/info.log',
    level:'info',
    format:customFormat
})
const error = new transports.File({
    filename:'logger/error.log',
    level:'error',
    format:customFormat
})
const log = new transports.Console({
    level:'info',
    format:customFormat
})

const logger = createLogger({
    transports:[info,error,log]
})

module.exports = logger;